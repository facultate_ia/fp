
#include <stdio.h>
int main()
{
	char c = 'z';
	char *cp = &c;
	printf("cp is 0x%08x\n", cp);
	printf("The character at cp is %c\n", *cp);
	/* Pointer arithmetic - see what cp+1 is */
	cp = cp + 1;
	printf("cp is 0x%08x\n", cp);

	/* Do not print *cp, because it points to memory space not allocated to your program */
	return 0;
}



*1 Deoarece char e reprezentat pe un byte, se va aduna 1 la adresa de memorie
		cp is 0x00def9b3
		The character at cp is Z
		cp is 0x00def9b4

*2  Deoarece int e reprezentat pe 4 byti, se va aduna 4 la adresa de memorie
		pi is 0x00fbf8b0
		The character at pi is 5
		pi is 0x00fbf8b4

*3 Deoarece double e reprezentat pe 8 byti, se va aduna 8 la adresa de memorie
		pd is 0x0090f974
		The character at pd is -858993459
		pd is 0x0090f97c
		
*4  E acelasi principiu...
