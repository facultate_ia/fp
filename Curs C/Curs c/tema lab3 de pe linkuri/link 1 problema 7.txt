/*
	Write a function that returns a pointer to the maximum value of an array of double's.  If the array is empty, return NULL.
*/

#include<stdio.h>
double* maximum(double* a, int size) {
	double *p; unsigned i; double max = -32000;
	if (size == 0) {
		p = NULL;
		return p;
	}
	else {
		for (i = 0; i < size; i++)
			if (*(a + i) > max)
				max = *(a + i);
		p = &max;
		return p;
	}
}
int main() {
	double array[] = {2,7,4 };
	double *p = array;
	unsigned n = sizeof(array) / sizeof(double);

	printf("%.1lf\n",*(maximum(p, n)));
	return 0;
}