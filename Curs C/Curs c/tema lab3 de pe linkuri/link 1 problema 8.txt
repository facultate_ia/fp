/*
	Write a function myStrLen(char*) which returns the length of the parameter cstring. 
	Write the function without using the C++ function strlen.
*/
#include <stdio.h>
int myStrLen(char *s) {
	int i = 0;
	while (*(s + i)) {
		i = i + 1;
	}
	return i;
}
int main() {
	char s[] = "Andrei";
	printf("The length is: %d \n", myStrLen(s));
	return 0;
}
