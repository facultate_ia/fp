#include <stdio.h>
#include <string.h>
#include <conio.h>

void inversare(char cuvant[], char *cuv_inversat)
{
	int n = strlen(cuvant);
	int m = 0;
	for (int i = n - 1; i >= 0; i--)
		cuv_inversat[m++] = cuvant[i];
	cuv_inversat[m] = '\0';
}

void main()
{
	char cuvant1[25];
	char cuvant2[25];
	printf("Introduceti cuvantul: ");
	scanf("%s", cuvant1);
	inversare(cuvant1, cuvant2);
	printf("Cuvantul %s inversat este %s\n", cuvant1, cuvant2);
	getch();
}