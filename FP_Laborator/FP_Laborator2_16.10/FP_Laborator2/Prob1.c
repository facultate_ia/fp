#include <stdio.h>
#include <conio.h>

float media(float a, float b)
{
	 return (a + b) / 2;
}

float medie(float v[100],int n)
{
	float s = 0;
	float ma;
	for (int i = 0; i < n; i++)
	{
		s += v[i];
	}
	ma = s / n;
	return ma;
}

int prim(int x)
{
	if (x < 2 || x>2 & x % 2 == 0)
		return 0;
	for (int d = 3; d*d <= x; d += 2)
		if (x%d == 0)
			return 0;
	return 1;
}

void main()
{
	char nume[30];
	float ma;
	float a;
	float b;
	float v[100];
	int n;
	int x;
	printf("Introduceti numele d-voastra: ");
	scanf("%s", &nume);
	printf("Hello, %s \n", nume);
	printf("\nIntroduceti primul nr: ");
	scanf("%f", &a);
	printf("Introduceti al doilea nr: ");
	scanf("%f", &b);
	ma = media(a, b);
	printf("Media celor doua numere este: %f \n", ma);
	printf("\nIntroduceti nr de elemente ale vectorului: ");
	scanf("%d", &n);
	for (int i = 0; i < n; i++)
	{
		printf("Introduceti v[%d]: ", i);
		scanf("%f", &v[i]);
	}
	ma = medie(v, n);
	printf("Media celor %d elemente este: %f\n", n, ma);
	printf("\nCele %d elemente sunt: \n", n);
	for (int i = 0; i < n; i++)
		printf("v[%d] = %f \n", i, v[i]);
	printf("\nIntroduceti nr pt a verifica daca este prim: ");
	scanf("%d", &x);
	if (prim(x))
		printf("Nr %d este prim!\n\n", x);
	else
		printf("Nr %d nu este prim!\n\n", x);
	getch();
}