#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>

int verificare(char nume[])
{
	for (int i = 0; i < strlen(nume); i++)
	{
		if (!isalpha(nume[i]))
		{
			return 0;
		}
	}
	return 1;
}

float media(float a,float b)
{
	return (a + b) / 2;
}

float media_n(float v[100],int n)
{
	int s = 0;
	for (int i = 0; i < n; i++)
	{
		s += v[i];
	}
	return s /(float) n;
}

void inversare(char cuvant1[],char cuvant2[])
{
	int n = strlen(cuvant1);
	for (int i =0; i<n; i++)
	{
		cuvant2[i] = cuvant1[n-i-1];
	}
	cuvant2[n] = '\0';
}

int palindrom(char cuvant[])
{
	int n = strlen(cuvant);
	for (int i = 0; i < n; i++)
	{
		if (toupper(cuvant[i]) != toupper(cuvant[n - i - 1]))
		{
			return 0;
		}
	}
	return 1;
}

void citire_fisier()
{
	char content[255];
	char *fname;
	FILE *f = fopen(fname, "r");
	if (!f)
	{
		printf("Fisierul %s nu aputut fi deschis", fname);
	}
	else
	{
		while (!foef(f))
		{
			fscanf("%s", content, sizeof(content));
		}
	}
	printf("%s", content);
	fclose(f);
}

void citire(int v[8])
{
	for (int i = 0; i < 8; i++)
	{
		scanf("%d", &v[i]);

	}
}

void siPeBiti(int v1[8], int v2[8], int *rez)
{
	for (int i = 0; i < 8; i++)
	{
		rez[i] = v1[i] & v2[i];
	}
}

void sauPeBiti(int v1[8], int v2[8], int *rez)
{
	for (int i = 0; i < 8; i++)
	{
		rez[i] = v1[i] | v2[i];
	}
}

void sauExclusivPeBiti(int v1[8], int v2[8], int *rez)
{
	for (int i = 0; i < 8; i++)
	{
		rez[i] = v1[i] ^ v2[i];
	}
}

void ShiftareDreapta(int v1[8], int *rez)
{
	for (int i = 0; i < 8; i++)
	{
		rez[i] = v1[i] >> 1;
	}
}

void ShiftareStanga(int v1[8], int *rez)
{
	for (int i = 0; i < 8; i++)
	{
		rez[i] = v1[i] << 1;
	}
}

void NotPeBiti(int v1[8], int *rez)
{
	for (int i = 0; i < 8; i++)
	{
		rez[i] = !v1[i];
	}
}

void afisare(int rez[8])
{
	for (int i = 0; i < 8; i++)
	{
		printf("%d", rez[i]);
	}
	printf("\n");
}

void afisareMeniuBiti()
{
	printf("Operatiile pe biti sunt:\n");
	printf("1. Si pe biti.\n");
	printf("2. Sau pe biti.\n");
	printf("3. Sau exclusiv pe biti.\n");
	printf("4. Shift-are la dreapta a primului vector.\n");
	printf("5. Shift-are la stanga a celui de-al doilea vector.\n");
	printf("6. Not pe biti a primului vector.\n");
}

void optiunea1()
{
	char nume[30];
	printf("Introduceti numele d-voastra: ");
	scanf("%s", &nume);
	while (!verificare(nume))
	{
		printf("Reintroduceti numele!\n");
		scanf("%s", &nume);
	}
	printf("Hello, %s \n", nume);
}

void optiunea2()
{
	float a, b, ma;
	printf("\nIntroduceti primul nr: ");
	scanf("%f", &a);
	printf("Introduceti al doilea nr: ");
	scanf("%f", &b);
	ma = media(a, b);
	printf("Media celor doua numere este: %f \n", ma);
}

void optiunea3()
{
	int n;
	float v[100], ma;
	printf("Introduceti nr de elemente: ");
	scanf("%d", &n);
	for (int i = 0; i < n; i++)
	{
		printf("Introduceti v[%d]: ", i);
		scanf("%f", &v[i]);
	}
	ma = media_n(v, n);
	printf("Media celor %d numere este: %f\n", n, ma);
}

void optiunea4()
{
	char cuvant1[30];
	char cuvant2[30];
	printf("Introduceti cuvantul: ");
	scanf("%s", cuvant1);
	inversare(cuvant1, cuvant2);
	printf("Cuvantul %s inversat este %s\n", cuvant1, cuvant2);
}

void optiunea5()
{
	char cuvant[30];
	printf("Introduceti cuvantul: ");
	scanf("%s", cuvant);
	if (palindrom(cuvant))
	{
		printf("Este palindrom!\n");
	}
	else
	{
		printf("Nu este palindrom!\n");
	}
}

void optiunea6()
{
	int op;
	int v1[8], v2[8];
	int rez[8];
	printf("Introduceti primul vector de nr binare:\n");
	citire(v1);
	printf("Introduceti al doilea vector de nr binare:\n");
	citire(v2);
	afisareMeniuBiti();
	printf("Alegeti o operatie!\n");
	scanf("%d", &op);
	switch (op)
	{
	case 1: 	siPeBiti(v1, v2, rez);	afisare(rez); break;
	case 2:		sauPeBiti(v1, v2, rez); afisare(rez);	break;
	case 3: 	sauExclusivPeBiti(v1, v2, rez); afisare(rez);	break;
	case 4: 	ShiftareDreapta(v1, rez); afisare(rez);	break;
	case 5: 	ShiftareStanga(v2, rez); afisare(rez);	break;
	case 6:		NotPeBiti(v1, rez); afisare(rez);	break;
	default: 	printf("Introduceti o optiune valida(de la 1 la 6)\n");		break;
	}
}

void afisareMeniu()
{
	printf("Optiunile sunt:\n");
	printf("Optiunea 1 este: Salut!\n");
	printf("Optiunea 2 este: Media a doua nr.\n");
	printf("Optiunea 3 este: Media a n nr.\n");
	printf("Optiunea 4 este: Inversarea unui cuvant.\n");
	printf("Optiunea 5 este: Verificare daca un cuvant este palindrom.\n");
	printf("Optiunea 6 este: Operatii pe biti.\n");
	printf("Optiunea 9 este: Iesire!\n\n");
}

void main()
{
	//int opt;
	//afisareMeniu();
	//do {
	//	printf("Introduceti optiunea dv.:");
	//	scanf("%d", &opt);
	//	switch (opt)
	//	{
	//	case 1: 	optiunea1();	break; 
	//	case 2:		optiunea2();	break; 
	//	case 3: 	optiunea3();	break;
	//	case 4: 	optiunea4();	break;
	//	case 5: 	optiunea5();	break;
	//	case 6:		optiunea6();	break;
	//	default: 	printf("Introduceti o optiune valida(de la 1 la 6 sau 9)\n");		break;	
	//	}
	//} while (opt != 9);
	citire_fisier();
}