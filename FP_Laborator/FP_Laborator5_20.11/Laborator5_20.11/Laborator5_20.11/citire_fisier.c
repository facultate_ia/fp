#include <stdio.h>
#include <string.h>
#include <conio.h>
#pragma warning(disable: 4996)
float medie_2_nr(float x, float y)
{
	return (x + y) / 2;
}
float medie_n_nr(int n, float sir[100])
{
	int i;
	float s = 0;
	for (i = 0; i < n; i++)
		s += sir[i];
	return s / n;
}
void inversare(char cuvant[], char *cuv_inversat)
{
	int n = strlen(cuvant);
	for (int i = 0; i < n; i++)
		cuv_inversat[i] = cuvant[n - 1 - i];
	cuv_inversat[n] = '\0';
}
void si(int v1[8], int v2[8], int rez[8], int nr_biti)
{
	int i;
	printf("Operatia &: \n");
	for (i = 0; i < nr_biti; i++)
		if (v1[i] == 1 && v2[i] == 1)
			rez[i] = 1;
		else rez[i] = 0;
	for (i = 0; i < nr_biti; i++)
		printf("%d", rez[i]);
	printf(" \n");
}
void sau(int v1[8], int v2[8], int rez[8], int nr_biti)
{
	int i;
	printf("Operatia |: \n");
	for (i = 0; i < nr_biti; i++)
		if (v1[i] == 1 || v2[i] == 1)
			rez[i] = 1;
		else rez[i] = 0;
	for (i = 0; i < nr_biti; i++)
		printf("%d", rez[i]);
	printf(" \n");
}
void sau_exclusiv(int v1[8], int v2[8], int rez[8], int nr_biti)
{
	int i;
	printf("Operatia ^: \n");
	for (i = 0; i < nr_biti; i++)
		if ((v1[i] == 1 && v2[i] == 0) || (v1[i] == 0 && v2[i] == 1))
			rez[i] = 1;
		else rez[i] = 0;
	for (i = 0; i < nr_biti; i++)
		printf("%d", rez[i]);
	printf(" \n");
}
void siftare_la_stanga(int v1[8], int v2[8], int rez[8], int nr_biti)
{
	int i;
	printf("Operatia <<: \n");
	for (i = 0; i < nr_biti - 1; i++)
		rez[i] = v1[i + 1];
	rez[nr_biti - 1] = 0;
	for (i = 0; i < nr_biti; i++)
		printf("%d", rez[i]);
	printf(" \n");
}
void siftare_la_dreapta(int v1[8], int v2[8], int rez[8], int nr_biti)
{
	int i;
	printf("Operatia >>: \n");
	rez[0] = v1[0];
	for (i = nr_biti - 1; i > 0; i--)
		rez[i] = v1[i - 1];
	for (i = 0; i < nr_biti; i++)
		printf("%d", rez[i]);
	printf(" \n");
}
void not(int v1[8], int v2[8], int rez[8], int nr_biti)
{
	int i;
	printf("Operatia ~: \n");
	for (i = 0; i < nr_biti; i++)
		if (v1[i] == 1)
			rez[i] = 0;
		else rez[i] = 1;
	for (i = 0; i < nr_biti; i++)
		printf("%d", rez[i]);
	printf(" \n");
}
void main()
{
	int opt = 10;
	printf(" 1.Afisarea numelui. \n");
	printf(" 2.Media a doua numere. \n");
	printf(" 3.Media a n numere. \n");
	printf(" 4.Inversarea unui cuvant. \n");
	printf(" 5.Verificare palindrom(cuvant). \n");
	printf(" 6. Operatii binare citire. \n");
	printf(" 7. Operatii binare fisier. \n");
	printf(" 9.Exit. \n");
	do
	{
		printf("Alegeti numarul algoritmului : ");
		scanf_s("%d", &opt);
		switch (opt)
		{
		case 1:
		{
			char nume[20];
			printf("Scrieti numele: \n");
			scanf_s("%s", nume, sizeof(nume));
			printf("Hello, %s !\n", nume);
			break;
		}
		case 2:
		{
			float x, y, m;
			printf("Scrieti doua numere: \n");
			scanf_s("%f%f", &x, &y);
			m = medie_2_nr(x, y);
			printf("Media numerelor %f si %f este %f \n", x, y, m);
			break;
		}
		case 3:
		{
			int n, i;
			float sir[100], m;
			printf("Introduceti numarul de numere: ");
			scanf_s("%d", &n);
			printf("Introduceti numerele: \n");
			for (i = 0; i < n; i++)
				scanf_s("%f", &sir[i]);
			m = medie_n_nr(n, sir);
			printf("Media este %f\n", m);
			break;
		}
		case 4:
		{
			char cuv1[25];
			char cuv2[25];
			printf("Introduceti cuvantul: \n");
			scanf_s("%s", cuv1, sizeof(cuv1));
			inversare(cuv1, cuv2);
			printf("Cuvantul %s inversat este %s \n", cuv1, cuv2);
			break;
		}
		case 5:
		{
			char cuv1[25];
			char cuv2[25];
			printf("Introduceti cuvantul: \n");
			scanf_s("%s", cuv1, sizeof(cuv1));
			inversare(cuv1, cuv2);
			if (strcmp(cuv1, cuv2) == 0)
				printf("Cuvantul %s este palindrom. \n", cuv1);
			else printf("Cuvantul %s nu este palindrom. \n", cuv1);
			break;
		}
		case 6:
		{
			int v1[8], v2[8], rez[8], nr_biti = 8, i;
			printf("Numarul a este: ");
			for (i = 0; i < nr_biti; i++)
				scanf_s("%d", &v1[i]);
			printf("Numarul b este: ");
			for (i = 0; i < nr_biti; i++)
				scanf_s("%d", &v2[i]);
			si(v1, v2, rez, nr_biti);
			sau(v1, v2, rez, nr_biti);
			sau_exclusiv(v1, v2, rez, nr_biti);
			siftare_la_dreapta(v1, v2, rez, nr_biti);
			siftare_la_stanga(v1, v2, rez, nr_biti);
			not(v1, v2, rez, nr_biti); break;
		}
		case 7:
		{
			int v1[8], v2[8], rez[8], nr_biti = 8, i;
			char *frame;
			FILE *f = NULL;
			frame = "in.txt";
			f = fopen(frame, "rb");
			if (!f)
			{
				printf("Fisierul %s nu a fost gasit!", frame);
			}
			else
			{
				for (i = 0; i < nr_biti; i++)
					fscanf(f, "%d", &v1[i]);
				for (i = 0; i < nr_biti; i++)
					fscanf(f, "%d", &v2[i]);
			}
			si(v1, v2, rez, nr_biti);
			sau(v1, v2, rez, nr_biti);
			sau_exclusiv(v1, v2, rez, nr_biti);
			siftare_la_dreapta(v1, v2, rez, nr_biti);
			siftare_la_stanga(v1, v2, rez, nr_biti);
			not(v1, v2, rez, nr_biti); break;
		}
		default:
		{
			printf("Optiunea aleasa nu este valida! \n");
			break;
		}
		}
	} while (opt != 9);

}